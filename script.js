//код, який я закоментував це спроба звернутись до API яке було в ТЗ...але в мене помилка 403

async function getIp() {
    try {
        let ipResp = await fetch('https://api.ipify.org/?format=json');
        let ipData = await ipResp.json();
        const ipAddres = ipData.ip;
        // console.log(ipAddres)
        // let locationResp = await fetch(`https://ip-api.com/json/${ipAddres}`)
        let locationResp = await fetch(`https://ipapi.co/${ipAddres}/json/`)
        let locData = await locationResp.json();
        // console.log(locData)
        let showResult = document.querySelector('.main');
        showResult.innerHTML = `
          <strong>Континент:</strong> ${locData.continent_code}<br>
          <strong>Країна:</strong> ${locData.country_name}<br>
          <strong>Регіон:</strong> ${locData.region}<br>
          <strong>Місто:</strong> ${locData.city}<br>
          <strong>Часовий пояс:</strong> ${locData.timezone}<br>
        `;

        // showResult.innerHTML = `
        // <strong>Континент:</strong> ${locData.continent}<br>
        // <strong>Країна:</strong> ${locData.country}<br>
        // <strong>Регіон:</strong> ${locData.regionName}<br>
        // <strong>Місто:</strong> ${locData.city}<br>
        // <strong>Район:</strong> ${locData.district}<br>
        // `;

    } catch (error) {
        console.error(error);
        alert('Сталася помилка. Спробуйте ще раз.');
    }

}